import "bootstrap/dist/css/bootstrap.css";
import "./app/styles/styles.css";

import { Router } from "./app/utils/router";
import { GameComponent } from "./app/components/game.component/game.component";
import { WelcomeComponent } from "./app/components/welcome.component/welcome.component";  
import { ScoreComponent } from "./app/components/score.component/score.component";  

const outlet = document.querySelector("#content-outlet");

const router = new Router(outlet)
  .register("", WelcomeComponent) 
  .register("game", GameComponent)
 .register("score", ScoreComponent); 

export function parseUrl() {
    debugger;
    const url = window.location;
    const query = url.href.split('?')[1] || '';
    const delimiter = '&';
    const parts = query
        .split(delimiter);

    return parts.map((item) => item.split('='))
        .reduce((acc, kv) => ({
            ...acc, [kv[0]]: kv[1]
        }), {});

}
